import java.util.ArrayList;

public class MoviePrinter {
    public static void main(String[] args) {
        ArrayList<String> movies = new ArrayList<>();

        movies.add("Pulp Fiction");
        movies.add("Lord of the Rings: Return of the King");
        movies.add("The Usual Suspects");

        for (String movie : movies) {
            System.out.println(movie);
        }
    }
}
