# last-cli-task

The last CLI task of the Java course.

## Compile the Source Code

![Compile](/images/compile.PNG)

## Build and Run the Jar

![Build and Run](/images/build-and-run-jar.PNG)
